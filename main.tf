// Choix du provider et definition des credentials et choix de la region

// Data source pour récuperer l'id AMI Ubuntu
// commande pour récuperer ID owner avec Amazon CLI : aws ec2 describe-images --image-ids ami-09a5c873bc79530d9  --region ap-southeast-2

data "aws_ami" "ubuntu1" {
  most_recent = true
  owners      = ["099720109477"]
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-20220912"]
  }
  provider = aws.region1

}


output "name" {
  value = data.aws_ami.ubuntu1.id

}


resource "aws_default_vpc" "default-vpc1" {
  tags = {
    Name = "Default VPC"
  }
  provider = aws.region1
}

// recuperer le 1er et le 2éme subnet existants sur AWS 

data "aws_subnets" "subnet1" {
  filter {
    name   = "vpc-id"
    values = [aws_default_vpc.default-vpc1.id]
  }
  provider = aws.region1

}

// Creation des instances dans deux subnets differents ( 4 instances dans chaque subnet)


resource "tls_private_key" "example" {
  algorithm = "RSA"
  rsa_bits  = 4096

}

resource "local_file" "foo" {
  content  = tls_private_key.example.private_key_pem
  filename = "./${var.ami_key_pair_name}"
}


resource "aws_key_pair" "generated_key" {
  key_name   = var.ami_key_pair_name
  public_key = tls_private_key.example.public_key_openssh
  provider   = aws.region1
}

resource "aws_instance" "ec2_instance1" {
  ami                    = data.aws_ami.ubuntu1.id
  instance_type          = var.instance_type
  subnet_id              = sort(data.aws_subnets.subnet1.ids)[0]
  key_name               = var.ami_key_pair_name
  vpc_security_group_ids = [aws_security_group.Security_groupe1.id]
  tags = {
    Name = "km-devops-${join("", [split("", "${var.Devops_engineer_name1[count.index]}")[0], substr(split(" ", "${var.Devops_engineer_name1[count.index]}")[1], 0, 2)])}"
  }
  count    = var.number_of_instances
  provider = aws.region1
}



// creation du sécurité groupe 

resource "aws_security_group" "Security_groupe1" {
  name        = "Security_groupe"
  description = "Allow TLS inbound traffic"
  vpc_id      = aws_default_vpc.default-vpc1.id

  ingress {
    description      = "TLS from VPC"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "Security_groupe"
  }
  provider = aws.region1
}

data "aws_internet_gateway" "gatway1" {
  filter {
    name   = "attachment.vpc-id"
    values = [aws_default_vpc.default-vpc1.id]
  }
  provider = aws.region1
}

// Creation et configuration du table de routage

resource "aws_route_table" "route1" {
  vpc_id = aws_default_vpc.default-vpc1.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = data.aws_internet_gateway.gatway1.id
  }
  tags = {
    Name = "route-table"
  }
  provider = aws.region1
}

// Association du subnets a la table de routage 

resource "aws_route_table_association" "association1" {
  subnet_id      = sort(data.aws_subnets.subnet1.ids)[0]
  route_table_id = aws_route_table.route1.id
  provider       = aws.region1
}

resource "aws_eip" "eip1" {
  vpc   = true
  count = 4
  tags = {
    Name = "eip-devops-${join("", [split("", "${var.Devops_engineer_name1[count.index]}")[0], substr(split(" ", "${var.Devops_engineer_name1[count.index]}")[1], 0, 2)])}"
  }
  provider = aws.region1
}

resource "aws_eip_association" "eip_assoc1" {
  instance_id   = aws_instance.ec2_instance1[count.index].id
  allocation_id = aws_eip.eip1[count.index].id
  count         = 4
  provider      = aws.region1
}


/* 

output "vpc" {
  
  value = aws_default_vpc.default.id
} */


/********************************************************************************************************************/





data "aws_ami" "ubuntu2" {
  most_recent = true
  owners      = ["099720109477"]
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-20220912"]
  }
  provider = aws.region2

}




resource "aws_default_vpc" "default-vpc2" {
  tags = {
    Name = "Default VPC"
  }
  provider = aws.region2
}

// recuperer le 1er et le 2éme subnet existants sur AWS 

data "aws_subnets" "subnet2" {
  filter {
    name   = "vpc-id"
    values = [aws_default_vpc.default-vpc2.id]
  }
  provider = aws.region2

}

resource "aws_key_pair" "generated_key1" {
  key_name   = var.ami_key_pair_name
  public_key = tls_private_key.example.public_key_openssh
  provider   = aws.region2
}

// Creation des instances dans deux subnets differents ( 4 instances dans chaque subnet)

resource "aws_instance" "ec2_instance2" {
  ami                    = data.aws_ami.ubuntu2.id
  instance_type          = var.instance_type
  subnet_id              = sort(data.aws_subnets.subnet2.ids)[0]
  key_name               = var.ami_key_pair_name
  vpc_security_group_ids = [aws_security_group.Security_groupe2.id]
  tags = {
    Name = "km-devops-${join("", [split("", "${var.Devops_engineer_name1[count.index + 4]}")[0], substr(split(" ", "${var.Devops_engineer_name1[count.index + 4]}")[1], 0, 2)])}"
  }
  count    = var.number_of_instances
  provider = aws.region2
}



// creation du sécurité groupe 

resource "aws_security_group" "Security_groupe2" {
  name        = "Security_groupe"
  description = "Allow TLS inbound traffic"
  vpc_id      = aws_default_vpc.default-vpc2.id

  ingress {
    description      = "TLS from VPC"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "Security_groupe"
  }
  provider = aws.region2
}

data "aws_internet_gateway" "gatway2" {
  filter {
    name   = "attachment.vpc-id"
    values = [aws_default_vpc.default-vpc2.id]
  }
  provider = aws.region2
}

// Creation et configuration du table de routage

resource "aws_route_table" "route2" {
  vpc_id = aws_default_vpc.default-vpc2.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = data.aws_internet_gateway.gatway2.id
  }
  tags = {
    Name = "route-table"
  }
  provider = aws.region2
}

// Association du subnets a la table de routage 

resource "aws_route_table_association" "association2" {
  subnet_id      = sort(data.aws_subnets.subnet2.ids)[0]
  route_table_id = aws_route_table.route2.id
  provider       = aws.region2
}

resource "aws_eip" "eip2" {
  vpc   = true
  count = 4
  tags = {
    Name = "eip-devops-${join("", [split("", "${var.Devops_engineer_name1[count.index + 4]}")[0], substr(split(" ", "${var.Devops_engineer_name1[count.index + 4]}")[1], 0, 2)])}"
  }
  provider = aws.region2
}

resource "aws_eip_association" "eip_assoc2" {
  instance_id   = aws_instance.ec2_instance2[count.index].id
  allocation_id = aws_eip.eip2[count.index].id
  count         = 4
  provider      = aws.region2
}


