variable "region1" {
}
variable "region2" {
}
variable "instance_type" {
}
variable "access_key" {
}
variable "secret_key" {
}

variable "instance_name" {
  description = "Name of the instance to be created"
  default     = "EC2_S4"
} 

variable "ami_key_pair_name" {
  default = "keypair"
}

variable "number_of_instances" {
  description = "number of instances to be created"
  default     = 4
}

variable "Devops_engineer_name1" {
  type = list(any)
  default = [

    "ElMehdi KAMIRI",
    "Abdelmalik AZAIZIA",
    "Dehia BOUDJEMAI",
    "Adama SYLLA",
    "Yassine ELFACHTALI",
    "Nabil OURDANI",
    "NedjmEddine ADJOU",
    "Lokmane GHOUDELBOURK"
  ]
}


